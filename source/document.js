const request = require('request-promise-native');


const Document = class {
    constructor(host, token) {
        this._host = host;
        this._token = token;
        this._endpoint = '/api/documents';
        this._batchEndpoint = '/api/documents/commands';
        this.FORMATS = ['md', 'html', 'json'];
    }

    get(secret, format = 'md') {
        if (!secret) {
            throw new Error('Get Document. Secret is missing.');
        }

        if (!this.FORMATS.includes(format)) {
            throw new Error(`Get Document. '${format}' format is not supported yet. Try ${this.FORMATS.join(',')}`);
        }

        return request.get({
            url: `https://${this._host}${this._endpoint}/${secret}`,
            headers: {
                'Authorization': `Token ${this._token}`,
                'X-Client': 'Unofficial JS'
            },
            qs: { format },
            json: true
        });
    }

    getBatch(secrets, format = 'md') {
        if (!this.FORMATS.includes(format)) {
            throw new Error(`Get Documents. '${format}' format is not supported yet. Try ${this.FORMATS.join(',')}`);
        }

        return request.post({
            url: `https://${this._host}${this._batchEndpoint}`,
            headers: {
                'Authorization': `Token ${this._token}`,
                'X-Client': 'Unofficial JS'
            },
            qs: { format },
            json: {
                command: 'get-documents',
                args: secrets
            }
        });
    }

    update(secret, content, format = 'md') {
        if (!secret) {
            throw new Error('Get Document. Secret is missing.');
        }

        if (!this.FORMATS.includes(format)) {
            throw new Error(`Get Document. '${format}' format is not supported yet. Try ${this.FORMATS.join(',')}`);
        }

        return request.put({
            url: `https://${this._host}${this._endpoint}/${secret}`,
            headers: {
                'Authorization': `Token ${this._token}`,
                'X-Client': 'Unofficial JS'
            },
            qs: { format },
            json: { content }
        })
    }
};


module.exports = Document;